class Job {
  final String location;
  final String title;
  final String company;
  final String position;
  final String salary;

  Job({
    required this.location,
    required this.title,
    required this.company,
    required this.position,
    required this.salary,
  });

  // Factory constructor to create a Job object from a JSON map
  factory Job.fromJson(Map<String, dynamic> json) {
    return Job(
      location: json['location'],
      title: json['title'],
      company: json['company'],
      position: json['position'],
      salary: json['salary'],
    );
  }

  // Method to convert a Job object to a JSON map
  Map<String, dynamic> toJson() {
    return {
      'location': location,
      'title': title,
      'company': company,
      'position': position,
      'salary': salary,
    };
  }
}
