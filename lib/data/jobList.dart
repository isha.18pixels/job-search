const joblist=

[
    {
        "location": "New York, NY",
        "title": "Software Engineer",
        "company": "Tech Corp",
        "position": "Full-Time",
        "salary": "\$120,000"
    },
    {
        "location": "San Francisco, CA",
        "title": "Product Manager",
        "company": "Innovate Inc.",
        "position": "Full-Time",
        "salary": "\$150,000"
    },
    {
        "location": "Chicago, IL",
        "title": "Data Scientist",
        "company": "DataWorks",
        "position": "Full-Time",
        "salary": "\$130,000"
    },
    {
        "location": "Austin, TX",
        "title": "UI/UX Designer",
        "company": "Creative Minds",
        "position": "Contract",
        "salary": "\$90,000"
    },
    {
        "location": "Seattle, WA",
        "title": "DevOps Engineer",
        "company": "Cloud Solutions",
        "position": "Full-Time",
        "salary": "\$125,000"
    },
    {
        "location": "Boston, MA",
        "title": "Business Analyst",
        "company": "BizAnalytica",
        "position": "Part-Time",
        "salary": "\$80,000"
    },
    {
        "location": "Los Angeles, CA",
        "title": "Front-End Developer",
        "company": "Web Wizards",
        "position": "Full-Time",
        "salary": "\$110,000"
    },
    {
        "location": "Denver, CO",
        "title": "Marketing Specialist",
        "company": "MarketGuru",
        "position": "Full-Time",
        "salary": "\$95,000"
    },
    {
        "location": "Miami, FL",
        "title": "Sales Manager",
        "company": "Sales Pros",
        "position": "Full-Time",
        "salary": "\$105,000"
    },
    {
        "location": "Dallas, TX",
        "title": "Network Administrator",
        "company": "NetSecure",
        "position": "Full-Time",
        "salary": "\$100,000"
    },
    {
        "location": "San Diego, CA",
        "title": "System Architect",
        "company": "TechDesign",
        "position": "Full-Time",
        "salary": "\$140,000"
    },
    {
        "location": "Phoenix, AZ",
        "title": "IT Support Specialist",
        "company": "HelpDesk Co.",
        "position": "Part-Time",
        "salary": "\$70,000"
    },
    {
        "location": "Philadelphia, PA",
        "title": "Database Administrator",
        "company": "DataKeepers",
        "position": "Full-Time",
        "salary": "\$120,000"
    },
    {
        "location": "Houston, TX",
        "title": "Operations Manager",
        "company": "BizOps",
        "position": "Full-Time",
        "salary": "\$115,000"
    },
    {
        "location": "Atlanta, GA",
        "title": "HR Specialist",
        "company": "PeopleFirst",
        "position": "Contract",
        "salary": "\$85,000"
    },
    {
        "location": "Washington, DC",
        "title": "Project Manager",
        "company": "ProjExpert",
        "position": "Full-Time",
        "salary": "\$125,000"
    },
    {
        "location": "Charlotte, NC",
        "title": "Accountant",
        "company": "FinancePro",
        "position": "Full-Time",
        "salary": "\$95,000"
    },
    {
        "location": "Detroit, MI",
        "title": "Mechanical Engineer",
        "company": "AutoWorks",
        "position": "Full-Time",
        "salary": "\$110,000"
    },
    {
        "location": "Orlando, FL",
        "title": "Electrical Engineer",
        "company": "PowerTech",
        "position": "Full-Time",
        "salary": "\$115,000"
    },
    {
        "location": "Minneapolis, MN",
        "title": "Quality Assurance Tester",
        "company": "QA Masters",
        "position": "Contract",
        "salary": "\$85,000"
    },
    {
        "location": "Portland, OR",
        "title": "Graphic Designer",
        "company": "DesignHub",
        "position": "Part-Time",
        "salary": "\$70,000"
    },
    {
        "location": "Las Vegas, NV",
        "title": "Customer Support Specialist",
        "company": "SupportPlus",
        "position": "Full-Time",
        "salary": "\$65,000"
    },
    {
        "location": "Cleveland, OH",
        "title": "SEO Specialist",
        "company": "WebRank",
        "position": "Full-Time",
        "salary": "\$80,000"
    },
    {
        "location": "San Jose, CA",
        "title": "Content Writer",
        "company": "WriteWell",
        "position": "Contract",
        "salary": "\$75,000"
    },
    {
        "location": "Columbus, OH",
        "title": "Cybersecurity Analyst",
        "company": "SecureNet",
        "position": "Full-Time",
        "salary": "\$120,000"
    },
    {
        "location": "Indianapolis, IN",
        "title": "Software Tester",
        "company": "TestIT",
        "position": "Full-Time",
        "salary": "\$85,000"
    },
    {
        "location": "Kansas City, MO",
        "title": "Machine Learning Engineer",
        "company": "AIML Tech",
        "position": "Full-Time",
        "salary": "\$135,000"
    },
    {
        "location": "Nashville, TN",
        "title": "Logistics Coordinator",
        "company": "LogiCo",
        "position": "Full-Time",
        "salary": "\$90,000"
    },
    {
        "location": "Oklahoma City, OK",
        "title": "Environmental Engineer",
        "company": "EcoTech",
        "position": "Full-Time",
        "salary": "\$110,000"
    },
    {
        "location": "Sacramento, CA",
        "title": "Civil Engineer",
        "company": "BuildIt",
        "position": "Full-Time",
        "salary": "\$105,000"
    }
];