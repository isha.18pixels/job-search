import 'package:flutter/material.dart';

import 'view/searchPage.dart';

void main() {
  runApp(JobSearchApp());
}

class JobSearchApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Job Search App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: JobSearchPage(),
    );
  }
}

